<?php
    require 'vendor/autoload.php';
    use MaxMind\Db\Reader;

    // Get visitor IP address
    $visitor_ip = $_SERVER['REMOTE_ADDR'];
    $h = gethostbyaddr($visitor_ip);
    if ($h != $visitor_ip) {
        $visitor_host = " (<code>$h</code>)";
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $visitor_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }

    // Process IP address
    if (isset($_GET['myip'])) {
        if (isset($_GET['raw'])) {
            echo $visitor_ip;
            return;
        }
        header('Access-Control-Allow-Origin: *');
        header('content-type: application/json; charset=utf-8');
        echo json_encode(array('ip' => $visitor_ip, 'host' => gethostbyaddr($visitor_ip)));
        return;
    } else if (isset($_GET['ip']) && $_GET['ip'] !== '') {
        $ip      = trim($_GET['ip']);
        $ipclean = htmlspecialchars($ip);

        if (filter_var($ip, FILTER_VALIDATE_IP)) {
            if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE)) {
                $err         = "Désolée, ".$ipclean." est une adresse IP privée.";
                $description = $ipclean." : désolée, c’est une adresse IP privée.";
            } else if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE)) {
                $err         = "Désolée, ".$ipclean." est une adresse IP réservée (RFC 6890).";
                $description = $ipclean." : désolée, c’est une adresse IP réservée (RFC 6890).";
            } else {
                $reader  = new Reader('dbip-city-lite-latest.mmdb');
                $infos   = $reader->get($ip);
                $reader->close();

                $h = gethostbyaddr($ip);
                if ($h == $ip) {
                    $h = "Désolée, pas d’enregistrement DNS inverse trouvé.";
                }
                $infos['reversedns'] = $h;

                $country_code = $infos['country']['iso_code'];
                $flag = mb_convert_encoding( '&#' . ( 127397 + ord( $country_code[0] ) ) . ';', 'UTF-8', 'HTML-ENTITIES') .
                        mb_convert_encoding( '&#' . ( 127397 + ord( $country_code[1] ) ) . ';', 'UTF-8', 'HTML-ENTITIES');
                if ($infos != null) {
                    $description = $ipclean." : ".$infos['continent']['names']['fr'].'/'.$infos['country']['names']['fr'].' '.$flag.'/'.$infos['city']['names']['en'];
                } else {
                    $err         = "Désolée, pas d’information pour l’adresse".$ipclean.".";
                    $description = $ipclean." : désolée, pas d’information pour cette adresse";
                }
            }
        } else {
            $err = "Désolée, ".$ipclean." n’est pas une adresse IP valide.";
        }
    } else {
        // Request is made with curl or wget (or with modified user agent header)
        if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match("/(curl|wget)/i", $_SERVER['HTTP_USER_AGENT'])) {
            echo $visitor_ip;
            return;
        }

        $description = "Pour voir les infos géographiques d’une adresse IP";
    }

    // Want JSON output
    if (isset($_GET['json'])) {
        header('content-type: application/json; charset=utf-8');
        if (isset($err)) {
            echo json_encode(['err' => $err]);
        } else {
            echo json_encode($infos);
        }
        return;
    }
    $other_ip_url = (str_contains($visitor_ip, ':')) ? 'https://ipv4.framasoft.org' : 'https://ipv6.framasoft.org';
    $visitor_ip = htmlspecialchars($visitor_ip);
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <title>FramIPLookup</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8" />
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <link rel="shortcut icon" href="/img/favicon.png">
        <link rel="icon" type="image/png" href="/img/favicon.png">
        <link rel="apple-touch-icon" href="/img/icon.png">
        <meta property="og:type" content="website">
        <meta property="og:title" content="Framasoft IP Lookup">
        <meta property="og:description" content="<?php echo $description ?>">
        <meta property="og:url" content="https://ip.framasoft.org">
        <meta property="og:image" content="https://ip.framasoft.org/img/icon.png">
        <link rel="stylesheet" href="css/iplookup.min.css">
    </head>
    <body>
        <div class="container">
            <main role="main">
                <h1>GeoIP lookup</h1>
                <section id="result">
                    <?php if (isset($_GET['ip']) && $_GET['ip'] !== '') {
                        if (isset($err)) {
                            echo "<p class=\"error\">$err</p>";
                        } else { ?>
                            <p class="h2">Résultat pour l’adresse <?= $ipclean ?></p>
                            <dl>
                                <dt>Continent :</dt>
                                <dd><?= $infos['continent']['names']['fr'] ?></dd>
                                <dt>Pays :</dt>
                                <dd><?= $infos['country']['names']['fr'].' '.$flag ?></dd>
                                <dt>Ville :</dt>
                                <dd><?= $infos['city']['names']['en'] ?></dd>
                                <dt>Enregistrement DNS inverse :</dt>
                                <dd><?= $infos['reversedns'] ?></dd>
                            </dl>
                        <?php }
                        echo '<hr>';
                    } ?>
                </section>
                <section id="form">
                    <form method="GET">
                        <p>
                            <label for="ip">Adresse IP à rechercher</label>
                            <?php if (isset($ipclean)) { ?>
                                <input type="text" name="ip" id="ip" value="<?= $ipclean ?>" autofocus required>
                            <?php } else { ?>
                                <input type="text" name="ip" id="ip" placeholder="<?= $visitor_ip; ?>" autofocus required>
                            <?php } ?>
                        </p>
                        <p>
                            <input type="checkbox" name="json" id="json">
                            <label for="json">Résultat sous forme de fichier JSON</label>
                        </p>
                        <p>
                            <input type="submit" value="Interroger la base de données !">
                        </p>
                    </form>
                </section>
                <section id="visitor">
                    <p id="visitor-ip">
                    Votre adresse IP est <a href="/?ip=<?= $visitor_ip ?>"><code><?= $visitor_ip ?></code></a><?= $visitor_host ?>.</small>
                    </p>
                <section>
            </main>
            <footer>
                <p>
                    <small><a href="https://framagit.org/framasoft/geoip-lookup" title="Dépôt Git de cette page">GeoIP lookup, AGPLv3</a></small>
                </p>
            </footer>
        </div>
        <script>
            async function fetchMyIP() {
                const response = await fetch('<?= $other_ip_url ?>?myip')
                return response.json()
            }
            fetchMyIP().then((data) => {
                if (data.ip != data.host) {
                    document.getElementById('visitor-ip').innerHTML = `Vos adresses IP sont <a href="/?ip=<?= $visitor_ip ?>"><code><?= $visitor_ip ?></code></a><?= $visitor_host ?> et <a href="/?ip=${data.ip}"><code>${data.ip}</code></a> (<code>${data.host}</code>).`
                } else {
                    document.getElementById('visitor-ip').innerHTML = `Vos adresses IP sont <a href="/?ip=<?= $visitor_ip ?>"><code><?= $visitor_ip ?></code></a><?= $visitor_host ?> et <a href="/?ip=${data.ip}"><code>${data.ip}</code></a>.`
                }
            })
        </script>
    </body>
</html>
