# GeoIP lookup

Une simple page PHP pour voir les informations geoIP relatives à une adresse IP.

## Installation

Vous aurez besoin de [`composer`](https://getcomposer.org/download/).

```
git clone https://framagit.org/framasoft/geoip-lookup
cd geoip-lookup
composer install
curl --remote-name https://dbip.mirror.framasoft.org/files/dbip-city-lite-latest.mmdb
```

Vérifiez que la base de données est valide :
```
curl -s https://dbip.mirror.framasoft.org/files/dbip-city-lite-latest.mmdb.sha512sum | sha512sum -c
```

## Mise à jour de la base de données

À mettre dans une tâche cron, une fois par mois après le 2 du mois :

```
mv dbip-city-lite-latest.mmdb dbip-city-lite-latest.mmdb.bak && \
  curl --remote-name https://dbip.mirror.framasoft.org/files/dbip-city-lite-latest.mmdb && \
  curl -s https://dbip.mirror.framasoft.org/files/dbip-city-lite-latest.mmdb.sha512sum | sha512sum -c && \
  rm dbip-city-lite-latest.mmdb.bak || \
  mv dbip-city-lite-latest.mmdb.bak dbip-city-lite-latest.mmdb
```

## Utilisation de serveurs Framasoft

Pour déterminer l’adresse IPv4 ou IPV6 du visiteur qui n’est pas utilisée lors de la consultation de la page, les serveurs `https://ipv4.framasoft.org` et `https://ipv6.framasoft.org` sont utilisés.

Pour utiliser vos propres serveurs, il vous suffit de modifier les adresses dans le fichier `index.php`.
Pour fournir le même service, il suffit d’utiliser cette page derrière des sites accessibles uniquement accessibles en IPv4 ou IPv6.

## Licence

Ce logiciel est diffusé sous les termes de la [licence Affero GPL v3](LICENSE).

La base de données de [DB-IP](https://db-ip.com/) utilisée est diffusée sous les termes de la [licence CC-BY](http://creativecommons.org/licenses/by/4.0/) et *mirrorée* par [Framasoft](https://framasoft.org) à l’adresse <https://dbip.mirror.framasoft.org/>.

La bibliothèque [MaxMind-DB-Reader-php](https://github.com/maxmind/MaxMind-DB-Reader-php) est diffusée sous les termes de la [licence Apache 2.0](https://github.com/maxmind/MaxMind-DB-Reader-php/blob/main/LICENSE).

Le framework CSS [Chota](https://jenil.github.io/chota/), diffusé sous les termes de la [licence MIT](https://github.com/jenil/chota/blob/master/LICENSE) a été utilisé pour créer la feuille de style du logiciel.
